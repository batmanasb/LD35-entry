#notes
#	
#	
#	
#	
#	
#	


extends RigidBody2D

#forms
onready var shapeShiftParticles = get_node("ShapeShiftParticles")
const climber = 0
const ball = 1
const anchor = 2
const ghost = 3
var currentForm = climber
var nextForm = currentForm
var prevForm = currentForm
var shapeShiftDelay = 3.0
var shapeShiftTimer = 0.0

#proximity
onready var proximitySensor = get_node("proximitySensor")
var proximityMonitor = 0

#movement
var jumpVector = Vector2(0,-200)
var sideImpulse = Vector2(100, 0)
var turnSpeed = deg2rad(80.0)

#climber stats
var climberBounce = 0.9
var climberFriction = 1.0
var climberMass = 1.0

#ball stats
var ballSpeedBoost = 1.2
var ballBounce = 0.2
var ballFriction = 0.1
var ballMass = 1.0

#anchor stats
var anchorBounce = 0.1
var anchorFriction = 0.3
var anchorMass = 20.0

#ghost mode
onready var ghostParticles = get_node("GhostParticles")
var gravity
var isGhosting = false
var isInsideWall = false
var ghostModeDuration = 4.0
var ghostModeTimer = ghostModeDuration
var minActivationSpeed = 50.0
var minGhostSpeed = 15.0

#camera
onready var camera = get_node("Camera2D")
var defaultSize
var defaultZoom = 0.5

#sprite
onready var sprite = get_node("Sprite")

#respawning
onready var respawningParticles = get_node("RespawningParticles")
var isRespawning = false
var lerpRate = 2.0
var spawnPoint
var respawnDistance = 200.0

#sound effects
onready var samplePlayer = get_node("SamplePlayer")

#button states
var isButtonPressed = true
var isMuted = false
var isFullscreen = false

#levels
var winLevel = 999
var maxLevel = 4


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	
	#connect signals
	connect("body_enter", self, "onCollision")
	proximitySensor.connect("body_enter", self, "enterProximity")
	proximitySensor.connect("body_exit", self, "exitProximity")
	
	#find and connect kill zone and goal
	get_node("../KillZone").connect("area_enter", self, "onEnterKillZone")
	get_node("../Goal").connect("area_enter", self, "enterGoal")
	
	#camera setup
	get_tree().get_root().connect("size_changed",self,"onResize")
	defaultSize = get_viewport_rect().size.y/defaultZoom
	onResize()
	
	#save default values
	gravity = get_gravity_scale()
	spawnPoint = get_global_pos()


#adjust the camera zoom when the viewport is resized
func onResize():
	var zoom = defaultSize/get_viewport_rect().size.y
	camera.set_zoom(Vector2(zoom,zoom))


func _fixed_process(delta):
	if(Input.is_action_pressed("quit")):
		get_tree().quit()
	elif(Input.is_action_pressed("fullscreen")):
		if(not isButtonPressed):
			isButtonPressed = true
			isFullscreen = not isFullscreen
			OS.set_window_fullscreen(isFullscreen)
	elif(Input.is_action_pressed("restart")):
		respawn()
	elif(Input.is_action_pressed("skip")):
		if(not isButtonPressed):
			isButtonPressed = true
			changeLevel(1)
	elif(Input.is_action_pressed("goBack")):
		if(not isButtonPressed):
			isButtonPressed = true
			changeLevel(-1)
	elif(Input.is_action_pressed("mute")):
		if(not isButtonPressed):
			isButtonPressed = true
			isMuted = not isMuted
			musicPlayer.setMute(isMuted)
			print(get_global_pos())
			print(get_pos())
	else:
		isButtonPressed = false
	
	if(isRespawning):
		handleRespawning(delta)
	else:
		handleInput(delta)
		handleShapeShift()
		handleGhostMode(delta)


func handleInput(delta):
	#only climber and ball can turn
	if(currentForm == climber or currentForm == ball):
		if(Input.is_action_pressed("left") and not Input.is_action_pressed("right")):
			set_angular_velocity(-turnSpeed)
			if(currentForm == climber):
				apply_impulse(Vector2(), -sideImpulse * delta)
			elif(currentForm == ball):
				apply_impulse(Vector2(), -sideImpulse * ballSpeedBoost * delta)
		if(Input.is_action_pressed("right") and not Input.is_action_pressed("left")):
			set_angular_velocity(turnSpeed)
			if(currentForm == climber):
				apply_impulse(Vector2(), sideImpulse * delta)
			elif(currentForm == ball):
				apply_impulse(Vector2(), sideImpulse * ballSpeedBoost * delta)
	
	#if shape shifting is off cooldown
	if(shapeShiftTimer < 0.0):
		
		#shapeshift
		if(currentForm != ghost):
			if(Input.is_action_pressed("shiftToClimber")):
				#can't shift into a climber unless their is enough space to do so!
				if(proximityMonitor == 0):
					nextForm = climber
			elif(Input.is_action_pressed("shiftToBall")):
				nextForm = ball
			elif(Input.is_action_pressed("shiftToGhost")):
				if(get_linear_velocity().length() > minActivationSpeed):
					nextForm = ghost
			elif(Input.is_action_pressed("shiftToAnchor")):
				#can't shift into an anchor unless their is enough space to do so!
				if(proximityMonitor == 0):
					nextForm = climber
					nextForm = anchor
	
	else:
		shapeShiftTimer -= delta


func handleShapeShift():
	if(currentForm != nextForm):
		#reset cooldown
		shapeShiftTimer = shapeShiftDelay
		
		#update form variables
		prevForm = currentForm
		currentForm = nextForm
		
		#set sprite to match
		sprite.set_frame(currentForm)
		
		#play shape shift effect (unless ghosting)
		if(currentForm != ghost):
			shapeShiftParticles.set_emitting(true)
			
			#play shapeshift effect
			samplePlayer.play("shapeShift")
		
		else:
			#play ghosting effect
			samplePlayer.play("ghosting")
		
		if(currentForm != climber):
			setClimberAsTrigger(true)
		else:
			setClimberAsTrigger(false)
		if(currentForm != ball):
			setBallAsTrigger(true)
		else:
			setBallAsTrigger(false)
		if(currentForm != anchor):
			setAnchorAsTrigger(true)
		else:
			setAnchorAsTrigger(false)
		if(currentForm == ghost):
			isGhosting = true
			ghostParticles.set_emitting(true)
			set_gravity_scale(0.0)
			if(proximityMonitor > 0):
				isInsideWall = true
		else:
			isGhosting = false
			ghostParticles.set_emitting(false)
			isInsideWall = false
			set_gravity_scale(gravity)


func setClimberAsTrigger(enable):
	set_shape_as_trigger(0, enable)
	set_shape_as_trigger(1, enable)
	
	if(not enable):
		set_bounce(climberBounce)
		set_friction(climberFriction)
		set_mass(climberMass)


func setBallAsTrigger(enable):
	set_shape_as_trigger(2, enable)
	
	if(not enable):
		set_bounce(ballBounce)
		set_friction(ballFriction)
		set_mass(ballMass)


func setAnchorAsTrigger(enable):
	set_shape_as_trigger(3, enable)
	set_shape_as_trigger(4, enable)
	set_shape_as_trigger(5, enable)
	
	if(not enable):
		set_bounce(anchorBounce)
		set_friction(anchorFriction)
		set_mass(anchorMass)


func onCollision(body):
	#climber can jump when falling and touching something
	if(currentForm == climber):
		if(Input.is_action_pressed("up") and get_linear_velocity().y <= 0.0):
			apply_impulse(Vector2(), jumpVector)


func enterProximity(body):
	proximityMonitor += 1


func exitProximity(body):
	proximityMonitor -= 1


func handleGhostMode(delta):
	if(isGhosting):
		#respawn if player gets stuck inside a wall
		if(get_linear_velocity().length() < minGhostSpeed):
			respawn()
		
		#lock in ghost mode once entered into a wall
		if(proximityMonitor > 0):
			isInsideWall = true
		
		#ghost mode times out if not inside of a wall
		if(not isInsideWall):
			ghostModeTimer -= delta
			if(ghostModeTimer < 0):
				ghostModeTimer = ghostModeDuration
				nextForm = prevForm
		
		#unghost after exiting a wall
		elif(proximityMonitor == 0):
			nextForm = prevForm
		



func onEnterKillZone( area ):
	respawn()


func respawn():
	#play respawn effect
	samplePlayer.play("respawning")
	
	#configure for respawning
	sprite.set_frame(ghost)
	currentForm = ghost
	set_gravity_scale(0.0)
	isRespawning = true
	respawningParticles.set_emitting(true)
	setClimberAsTrigger(true)
	setBallAsTrigger(true)
	setAnchorAsTrigger(true)


func handleRespawning(delta):
	if(get_global_pos().distance_to(spawnPoint) > respawnDistance):
		var pos = get_global_pos()
		pos.x = lerp(pos.x, spawnPoint.x, lerpRate*delta)
		pos.y = lerp(pos.y, spawnPoint.y, lerpRate*delta)
		set_global_pos(pos)
	else:
		isRespawning = false
		respawningParticles.set_emitting(false)
		nextForm = climber
		set_linear_velocity(Vector2())
		set_angular_velocity(0.0)
		handleShapeShift()
		shapeShiftTimer = 0.0


func enterGoal(body):
	changeLevel(1)


func changeLevel(increment):
	var num = get_parent().get_name().to_int()
	
	if(num+increment > winLevel):
		num = 0
		increment = 0
	if(num+increment == winLevel-1):
		num = maxLevel
		increment = 0
	elif(num+increment > maxLevel):
		num = winLevel
		increment = 0
	elif(num+increment < 0):
		increment = 0
	
	var nextLevel = str("res://levels/level",num+increment,".tscn")
	
	get_tree().change_scene(nextLevel)