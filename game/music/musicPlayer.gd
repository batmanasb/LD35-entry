
extends StreamPlayer

var bgDrumBeat = preload("res://music/bgDrumBeat.ogg")

func _ready():
	set_volume_db(10.0)
	set_stream(bgDrumBeat)
	set_loop(true)
	play()


func setMute(enable):
	if(enable):
		stop()
	else:
		play()
